extern crate cpp_build;

use std::env::var;

fn main()
{
	// Search paths for the C++ exiv2 precompiled lib + headers
	let manifest_dir = var("CARGO_MANIFEST_DIR").unwrap();
	
	let include_path = format!("{}/exiv2/include/", manifest_dir);
	let lib_path = format!("{}/exiv2/lib/", manifest_dir);
	
	// Compile our lib.rs wrapper using the cpp_build machinery
	cpp_build::Config::new()
		.include(include_path)
		.build("src/lib.rs");
	
	// Look for the precompiled static lib
	println!("cargo:rustc-link-search={}", lib_path);
	println!("cargo:rustc-link-lib=exiv2");
}
