exiv2_copy_rs
================

Combines the exiv2 precompiled static library with a Rust wrapper,
to form a new static library.

About
-----

The basic usage of the exiv2 library is copied from:
"samples/metacopy.cpp"

The original repo for this project can be found at:
https://bitbucket.org/Aligorith/exif2-copy-rs


Compiling/exiv2-Updating Instructions
-------------------------------------

By default, exiv2 for Windows (64-bit, MSVC) is included in the exiv2_lib folder
already. The version included is:
    exiv2-0.27.2-2017msvc64

Downloaded from the official website (https://www.exiv2.org/download.html).
According to the text there, the builds provided on that page are the ones for
the "shared libraries" (i.e. will require the .dll to be present beside the executable),
and that static builds will need to be manually compiled from source.


To update/compile this crate for other platforms, copy the compiled exiv2 libs + headers into the "exiv2_lib" folder as follows:

<pre>
 - [root]/
      - exiv2/
          - bin/
              - exiv2.dll
          - include/
              - [exiv2]/include/
          - lib/
              - [exiv2]/lib/exib2.lib
              - [exiv2]/lib/exib2-xmp.lib
</pre>

To compile this crate:
* On Windows, ensure that the MSVC build environment is set (e.g. in a shell that has had vcvars64.bat run in it)

* "cargo build" should build it (if everything else is in order)
  "cargo build --release" for the final executable.


Usage
-----

In the code that needs to copy EXIF and other image data between two image files,
do something like:

```
extern crate exiv2_copy_rs;

fn process_images()
{
	let src_file: &str = ...;
	let dst_file: &str = ...;
	
	... do the image data processing/copying ...
	
	let success = exiv2_copy_rs::copy_exif_data(src_file, dst_file);
	if success {
		println!("Copied EXIF for {} -> {}", &src_file, &dst_file);
	}
}

```

