#![recursion_limit="256"] // Otherwise, the compiling the main cpp macro fails

#[macro_use]
extern crate cpp;

// Note: The includes here are based on those used for exiv2's samples/metacopy.cpp
cpp!{{
	#include <exiv2/exiv2.hpp>
	
	#include <iostream>
	#include <fstream>
	#include <cassert>
	#include <string>
}}

pub fn copy_exif_data(in_file: &str, out_file: &str) -> bool
{
	// Wrap the two string values using the Fixed-Function API types
	let in_file_ffi = std::ffi::CString::new(in_file).unwrap();
	let in_file_ptr = in_file_ffi.as_ptr();
	
	let out_file_ffi = std::ffi::CString::new(out_file).unwrap();
	let out_file_ptr = out_file_ffi.as_ptr();
	
	let r = unsafe {
		cpp!([in_file_ptr as "const char *",
			  out_file_ptr as "const char *"] 
			 -> u32 as "int32_t" 
		{
			// The exiv2/C++ code here is based on exiv2's samples/metacopy.cpp
			try {
				// Convert in_file and out_file names from const char* to std::string
				std::string in_file_str(in_file_ptr);
				std::string out_file_str(out_file_ptr);
				
				// Load source file + read its metadata
				auto readImg = Exiv2::ImageFactory::open(in_file_str);
				assert(readImg.get() != 0);
				readImg->readMetadata();
				
				// Load output file
				auto writeImg = Exiv2::ImageFactory::open(out_file_str);
				assert(writeImg.get() != 0);
				
				// Copy over all metadata, preserving any that was there already
				writeImg->readMetadata(); // preserve existing
				
				writeImg->setIptcData(readImg->iptcData());
				writeImg->setExifData(readImg->exifData()); // The main one for EXIF
				writeImg->setComment(readImg->comment());
				writeImg->setXmpData(readImg->xmpData());
				
				// Actually write the metadata now
				try {
					writeImg->writeMetadata();
				}
				catch (const Exiv2::AnyError&) {
					std::cerr << "copy_exif_data()" <<
						": Could not write metadata to (" << out_file_str << ")\n";
					return 8;
				}
			}
			catch (Exiv2::AnyError& e) {
				std::cerr << "Caught Exiv2 exception '" << e << "'\n";
				return 10;
			}
			
			return 0;
		})
	};
	
	// Returns success of the operation this way.
	// Other error codes are not handled yet.
	return r == 0;
}
