exiv2-copy-rs
=============

This project contains an attempt to produce a specialised Rust wrapper around the exiv2 C++ library.
Instead of wrapping the entire generic API, this only wraps the subset of the API that we actually
need - i.e. extracting all the EXIF from a named file, and then dumping it into a second file.

Since this is a very specialised operation, we don't even need to fully wrap the exiv2 types.
Instead, we can get away with defining a C++ function that uses the exiv2 library to perform
this operation (taking 2 strings - one for the input path, one for the output path), then exposing
the resulting function to Rust.

Original repo:
https://bitbucket.org/Aligorith/exif2-copy-rs

Original Author:
Joshua Leung (aligorith@gmail.com)

March 2020

